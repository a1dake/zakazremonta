import sys
from bs4 import BeautifulSoup
import requests
from lxml import html
sys.path.append("/home/manage_report")
#from Send_report.mywrapper import magicDB

class Parser:
    def __init__(self,  parser_name: str):
        self.result_data: dict = {'name': parser_name,
                                  'data': []}

    #@magicDB
    def run(self):
        content: list = self.get_content()
        self.result_data['data'] = content
        print(content)
        return self.result_data

    def get_urls(self):
        session = requests.Session()
        session.headers.update({
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'User-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.52',
        })
        ads = []
        cities = ['moscow', 'sankt-peterburg', 'abakan', 'barnaul', 'belgorod', 'volgograd', 'voronezh', 'ekaterinburg', 'kazan', 'krasnodar', 'krasnoyarsk', 'nijnii-novgorod', 'novosibirsk', 'omsk', 'orel', 'perm', 'rostov-on-don', 'samara', 'saratov', 'stavropol', 'tula', 'ufa', 'habarovsk', 'chelyabinsk', 'yaroslavl']

        for city in cities:
            url = f'https://www.zakazremonta.ru/catalog/{city}/?p=1'
            response = session.get(url, timeout=30)
            soup = BeautifulSoup(response.text, 'lxml')
            market = soup.select_one('div', class_='task-list pagination-container pagged-list')

            for items in market.find_all('div', class_='task-item catalog-order-list'):
                new_url = items.find('div', class_='task-item-title').find('a').get('href')
                new_date = items.find('div', class_='task-item-date').get_text().strip()
                if 'мин' in new_date or 'час' in new_date:
                    url = 'https://www.zakazremonta.ru' + new_url
                    ads.append(url)
        return ads       

    def get_content(self):
        ads = self.get_urls()
        contents = []
        for link in ads:
            item_data = {
                'type': 1,
                'title': '',
                'description': '',
                'rubric': '',
                'contact': '',
                'phone': '',
                'email': '',
                'region': '',
                'city': '',
                'url': '',
                }

            try:
                session = requests.Session()
                session.headers.update({
                    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                    'User-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.52',
                })
                response = session.get(link, timeout=30)
                tree = html.document_fromstring(response.content)

                item_data['title'] = str(self.get_title(tree))
                item_data['description'] = str(self.get_desc(tree))
                item_data['url'] = str(link)
                item_data['contact'] = str(self.get_customer(tree))
                item_data['phone'] = str(self.get_phone(tree))
                item_data['city'] = str(self.get_region(tree))

                contents.append(item_data)

            except Exception as e:
                print(f"Страница с ошибкой: {e} - ", link)
        return contents
    
    def get_title(self, tree):
        try:
            data = ''.join(tree.xpath('//h1[@class="title"]/text()')).strip()
        except:
            data = ''
        return data
        
    def get_desc(self, tree):
        try:
            data = ' '.join(''.join(tree.xpath('//div[@class="task-item-descr"]/p/text()')).split())
        except:
            data = ''
        return data
    
    def get_customer(self, tree):
        try:
            data = ''.join(';'.join(tree.xpath('//td[@class="task-item-user"]/parent::*/./*[2]/text()[1]')).strip()).split('(')[0].strip()
        except:
            data = ''
        return data

    def get_phone(self, tree):
        try:
            data = ''.join(tree.xpath('//td[@class="task-item-phone"]/parent::*/./*[2]/text()[1]'))
        except:
            data = ''
        return data
        
    def get_region(self, tree):
        try:
            data = ' '.join(''.join(tree.xpath('//td[@class="task-item-city"]/parent::*/./*[2]/a/text()')).strip().split())
        except:
            data = ''
        return data
